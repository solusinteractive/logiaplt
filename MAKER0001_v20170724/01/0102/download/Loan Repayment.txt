Account Transfer Information

Bank Reference Number:   170724683919
From Account:            21102500074905 / AIRSPACE SERV
To Account:               71301000047159 / AZANIRHAB NII
Amount:                  "RM 4,356.00"
Creation Date:           24-07-2017 14:42:23
Payment Date:            30-08-2017
Total Charges:           RM 3.00
GST Charges:             RM 0.18 ** 
                         (** : The GST Charges is exclusive from the Charges)
Total Amount to Debit:   RM 4,359.18
Recipient's Reference:   SAMPLE#4356
Other Payment Details:   SAMPLE HARDWARE
Transfer Type:           Post Dated Payment
Post Dated Date:         30-08-2017
Frequency:               
Start Date:              
Total No Of Payments:    
Expiry Date:             


Workflow Information

User Name           Role                                    Status              Date & Time         
FITRI DATA ENTRY    Data Entry                              Done                24-07-2017 14:42:23 
FITRI AUTHORIZER    Authorizer                              Pending                                 
Shirin Authorizer   Authorizer                              Pending                                 
mira                Authorizer                              Pending                                 





