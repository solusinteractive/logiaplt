﻿(function ($, window, document) {

    // Global variables
    var $window = $(window),
		$document = $('html, body'),
		$html = $('html'),
		$body = $('body'),
		$sectionContainer = $('.section-container'),
		$section = $('.section'),
		$btnScroll = $('#btn-scroll'),
		mcsTop = 0,
		mcsTopStart = 0;

    function collapseMenu() {
        $body.removeClass('open-menu');
        $('header [data-toggle="collapse"]').addClass('collapsed');
        $('header .collapse').removeClass('in');
    }

    function adjustWindow() {

        $(window).on('load resize scroll', function (e) {

            // Get viewport size
            viewportWidth = $window.width();
            viewportHeight = $window.height();

            $sectionContainer.mCustomScrollbar({
                scrollInertia: 30,
                mouseWheel: {
                    scrollAmount: 100
                },
                autoHideScrollbar: true,
                callbacks: {
                    onScrollStart: function () {

                        mcsTopStart = -(this.mcs.top);

                    },
                    whileScrolling: function () {

                        mcsTop = this.mcs.top;
                        mcsTopPct = this.mcs.topPct;

                        if (mcsTopPct == 100) {
                            $btnScroll.addClass('last');
                            $('.section-title', $btnScroll).text('BACK TO TOP');
                        }
                        else {
                            sectionScroll();
                        }

                        if (-(mcsTop) > 80) {

                            if (-(mcsTop) > mcsTopStart) {
                                //$body.addClass('header-hide');
                                collapseMenu();
                            }
                            if (-(mcsTop) < mcsTopStart) {
                                $body.removeClass('header-hide');
                            }

                        }

                    },
                    onScroll: function () {

                        if (-(mcsTop) <= 80) {
                            $body.removeClass('header-hide');
                        }

                    },
                }
            });

            if ($sectionContainer.hasClass('mCS_no_scrollbar')) {
                $btnScroll.addClass('hide');
            }
            else {
                $btnScroll.removeClass('hide');
            }

        }).trigger('scroll');

    }

    function sectionScroll() {

        var sectionTop = -(mcsTop);

        $section.each(function (index) {
            if ($(this).position().top - viewportHeight / 2 > sectionTop) {
                if ($btnScroll.hasClass('last')) {
                    $btnScroll.removeClass('last');
                }
                $('.section-title', $btnScroll).text($(this).attr('data-section-title'));
                $btnScroll.attr('data-target', '#' + $(this).attr('id'));
                return false;
            }
            else if (index == $section.length - 1 && sectionTop >= $(this).position().top) {
                if (!$btnScroll.hasClass('last')) {
                    $btnScroll.addClass('last');
                    $btnScroll.removeAttr('data-target');
                }
                $('.section-title', $btnScroll).text('BACK TO TOP');
                return false;
            }
        });

    }

    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    $(function () {

        var scrollInertia = 1200;
        var nextSectionID = $('.section:nth-child(2)', $sectionContainer).attr('id');
        var nextSectionTitle = $('.section:nth-child(2)', $sectionContainer).attr('data-section-title');
        $btnScroll.attr('data-target', '#' + nextSectionID);
        $('.section-title', $btnScroll).text(nextSectionTitle);

        $('.section a[data-target]', $sectionContainer).on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            var dataTarget = $this.attr('data-target');
            $sectionContainer.mCustomScrollbar('scrollTo', dataTarget);
        });

        $('.profile-photo').imgLiquid({
            fill: false,
            verticalAlign: 'bottom',
            horizontalAlign: 'center'
        });

        $('#btn-scroll').on('click', function () {

            var $this = $(this);
            var dataTarget = $this.attr('data-target');

            if ($this.hasClass('last')) {
                $sectionContainer.mCustomScrollbar('scrollTo', 'top', { scrollInertia: scrollInertia });
            }
            else {
                if (dataTarget !== '#undefined') {
                    $sectionContainer.mCustomScrollbar('scrollTo', dataTarget, { scrollInertia: scrollInertia });
                }
                else {
                    $sectionContainer.mCustomScrollbar('scrollTo', 'bottom', { scrollInertia: scrollInertia });
                }
            }

        });

        var urlParam = window.location.search.substring(1);
        var sectionName = decodeURIComponent(getUrlParameter('section'));

        if (typeof sectionName !== typeof undefined && sectionName !== false) {
            var sectionID = '#' + $('[data-section-title="' + sectionName + '"]', $sectionContainer).attr('id');
            if ($html.hasClass('no-mobile')) {
                setTimeout(function () {
                    $sectionContainer.mCustomScrollbar('scrollTo', sectionID, { scrollInertia: scrollInertia });
                }, 300);
            }
            else if (sectionID != '#undefined') {
                setTimeout(function () {
                    $document.animate({
                        scrollTop: $(sectionID).offset()
                    }, scrollInertia);
                }, 300);
            }
        }

        adjustWindow();

    });

}(window.jQuery, window, document));