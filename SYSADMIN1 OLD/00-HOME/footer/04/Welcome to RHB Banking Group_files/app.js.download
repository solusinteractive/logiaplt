// IIFE - Immediately Invoked Function Expression
(function($, window, document) {

	var method;
	var noop = function () {};
	var methods = [
		'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
		'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
		'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
		'timeStamp', 'trace', 'warn'
	];
	var length = methods.length;
	var console = (window.console = window.console || {});

	while (length--) {
		method = methods[length];

		// Only stub undefined methods.
		if (!console[method]) {
			console[method] = noop;
		}
	}

	// Global variables
	var $window = $(window),
		$document = $('html, body'),
		$html = $('html'),
		$body = $('body'),
		uaParser = new UAParser().getResult(),
		$contentScrollable = $('.content-scrollable'),
		timeOut;

	// Global functions
	function collapseMenu() {
		$body.removeClass('open-menu');
		$('header [data-toggle="collapse"]').addClass('collapsed');
		$('header .collapse').removeClass('in');
	}

	function adjustWindow() {

		$(window).on('load resize scroll', function(e) {
		
			// Get viewport size
			viewportWidth = $window.width();
			viewportHeight = $window.height();

			// Keep minimum height 560
			if(viewportHeight <= 560) {
				viewportHeight = 560;
			}

			// viewport height
			$('.viewport-height').height(viewportHeight);
			$('.viewport-min-height').css('min-height', viewportHeight);

			// Get secondary size
			secondaryWidth = $('.secondary').width();
			secondaryHeight = $('.secondary').height();

			// minus secondary height
			$('.viewport-height-secondary').height(parseInt(viewportHeight - secondaryHeight));
			$('.viewport-min-height-secondary').css('min-height', parseInt(viewportHeight - secondaryHeight));

			$contentScrollable.mCustomScrollbar({
				scrollInertia: 30,
				mouseWheel: {
					scrollAmount: 100
				},
				autoHideScrollbar: true
			});

			// table responsive
			$('.table-responsive').each(function(i) {
				var $this = $(this);
				if($this.hasScrollBar()) {
					$this.addClass('scrollbar-active');
				}
				else {
					$this.removeClass('scrollbar-active');
				}
			});

			// reset header hide
			if(viewportWidth > 767) {
				$body.removeClass('header-hide');
			}

			// interchange image on responsive viewport
			if(viewportWidth < 768) {
				interchangeImg('small');
			}
			else if(viewportWidth >= 768) {
				interchangeImg('large');
			}

			// back top
			if($(this).scrollTop() > 300 ) { 
				$('.back-top').addClass('in');
			}
			else {
				$('.back-top').removeClass('in');
			}
			
		}).trigger('scroll');

	}

	function goBack() {
		window.history.back();
	}

	function slugify(str) {
		var $slug = '';
		var trimmed = $.trim(str);
		$slug = trimmed.replace(/[^a-z0-9-]/gi, '-').replace(/-+/g, '-').replace(/^-|-$/g, '');
		return $slug.toLowerCase();
	}

	function interchangeImg(type) {
		$('.bg > img').each(function(i) {
			var dataImg = $(this).data(type);
			if (typeof dataImg !== typeof undefined && dataImg !== false) {
				$(this).attr('src', dataImg);
				$('.bg').imgLiquid();
			}
		});
	}

	// function GetCookie(cname) {
	// 	var name = cname + "=";
	// 	var ca = document.cookie.split(';');
	// 	for(var i=0; i<ca.length; i++) {
	// 		var c = ca[i];
	// 		while (c.charAt(0)==' ') c = c.substring(1);
	// 		if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
	// 	}
	// 	return "";
	// }

	$.fn.hasScrollBar = function() {
		return this.get(0).scrollWidth > this.width();
	}

	$(function() {
		
		if(uaParser.browser.name){
			$html.addClass('ua-browser-'+uaParser.browser.name.toLowerCase());
		}
		if(uaParser.device.model){
			$html.addClass('ua-device-'+uaParser.device.model.toLowerCase());
		}
		if(uaParser.engine.name){
			$html.addClass('ua-engine-'+uaParser.engine.name.toLowerCase());
		}
		if(uaParser.os.name){
			$html.addClass('ua-os-'+uaParser.os.name.toLowerCase());
		}

		$('.section-container .section:first-child').addClass('section-first viewport-min-height-secondary');
		$('.section-container .section:last-child').addClass('section-last');

		$('#menu').on('click', function(e) {
			e.preventDefault();
			var $this = $(this);
			if(!$this.hasClass('disabled')) {
				$body.removeClass('open-menu');
				$('.menu-collapse > li [data-toggle="collapse"]').addClass('collapsed');
				$('.menu-collapse > li .collapse').removeClass('in');
			}
		});

		$('#menu-backdrop').on('click', function() {
			collapseMenu();
		});

		$('#menu-collapse').on('show.bs.collapse', function() {
			$body.addClass('open-menu');
			$('#menu').addClass('disabled');
		});

		$('#menu-collapse').on('shown.bs.collapse', function() {
			$body.addClass('open-menu');
			$('#menu').removeClass('disabled');
		});

		$('#menu-collapse .menu-panel').on('show.bs.collapse', function(e) {
			e.stopPropagation();  
		});

		$('#menu-collapse .menu-panel').on('shown.bs.collapse', function(e) {
			e.stopPropagation();  
		});

		$('#menu-collapse [data-toggle="collapse"]').on('click', function(e) {
			e.preventDefault();
		});

		$('#menu-collapse > li > a').on('click', function() {
			var $this = $(this);
			var dataToggle = $this.attr('data-toggle');
			if(dataToggle == 'collapse') {
				$('.menu-collapse .menu-panel > li > [data-toggle="collapse"]').addClass('collapsed');
				$('.menu-collapse .menu-panel > li > .collapse').removeClass('in');
			}
		});

		$('#menu-collapse .menu-panel li > a').on('click', function() {
			var $this = $(this);
			var dataToggle = $this.attr('data-toggle');
			if(dataToggle == 'collapse') {
				$this.parent().find('.menu-panel > li [data-toggle="collapse"]').addClass('collapsed');
				$this.parent().find('.menu-panel > li .collapse').removeClass('in');
			}
		});

		$('.menu-panel-carousel').on('shown.bs.collapse', function() {

			var $this = $(this);
			var menuPanelCarouselID = '#'+$this.attr('id');
			var menuCarousel = $('.menu-carousel', menuPanelCarouselID);
			var item480 = 1;
			var item768 = 1;
			var item1200 = 1;

			if($('.menu-carousel-container', menuPanelCarouselID).hasClass('menu-carousel-column-2')) {
				item480 = 2;
				item768 = 2;
				item1200 = 2;
			}
			else if($('.menu-carousel-container', menuPanelCarouselID).hasClass('menu-carousel-column-3')) {
				item480 = 2;
				item768 = 2;
				item1200 = 3;
			}
			else if($('.menu-carousel-container', menuPanelCarouselID).hasClass('menu-carousel-column-4')) {
				item480 = 2;
				item768 = 2;
				item1200 = 4;
			}

			menuCarousel.on('initialized.owl.carousel refreshed.owl.carousel resized.owl.carousel changed.owl.carousel drag.owl.carousel', function(event) {
				if(event.page.count == 1) {
					$('.owl-controls', menuCarousel).fadeOut();
				}
				else {
					$('.owl-controls', menuCarousel).fadeIn();
				}
			});

			menuCarousel.owlCarousel({
				margin: 30,
				items: 1,
				loop: false,
				nav: true,
				navText: ['<span class="rhb-glyph-icon-chevron-left"></span>','<span class="rhb-glyph-icon-chevron-right"></span>'],
				responsive: {
					0 : {
						items: 1
					},
					320 : {
						items: 1
					},
					480 : {
						items: item480
					},
					768 : {
						items: item768
					},
					1200 : {
						items: item1200
					}
				}
			});

			menuCarousel.trigger('to.owl.carousel', 0);
			menuCarousel.trigger('refresh.owl.carousel');
			
		});

		$('html.no-mobile #menu').hover(function() {
			var $this = $(this);
			if (!timeOut && $this.hasClass('collapsed') && viewportWidth > 750) {
				timeOut = window.setTimeout(function() {
					timeOut = null;
					if ($this.hasClass('collapsed')) {
						$this.click();
					}
				}, 300);
			}
		},
		function() {
			if (timeOut) {
				window.clearTimeout(timeOut);
				timeOut = null;
			}
		});

		$('html.no-mobile #menu-collapse li > a[data-toggle="collapse"]').hover(function() {
			var $this = $(this);
			if($this.hasClass('collapsed') && viewportWidth > 750) {
				timeOut = window.setTimeout(function() {
					if ($this.hasClass('collapsed')) {
						$this.click();
					}
				}, 20);
			}
		},
		function() {
			if (timeOut) {
				window.clearTimeout(timeOut);
				timeOut = null;
			}
		});

		$('html.no-mobile #menu-collapse li > a').not('html.no-mobile #menu-collapse li > a[data-toggle="collapse"]').hover(function() {
			var $this = $(this);
			var relatives = $this.parent().siblings();
			relatives.each(function(i) {
				var $this = $(this);
				$this.find('[data-toggle="collapse"]').addClass('collapsed');
				$this.find('.collapse').removeClass('in');
			});
		},
		function() {
			
		});

		$('.nav-menu-right a').on('click', function() {
			collapseMenu();
		});

		$('html.no-mobile header .nav-menu-right .dropdown > a[data-toggle="dropdown"]').hover(function() {
			var $this = $(this);
			if(!$this.parent().hasClass('open') && viewportWidth > 750) {
				timeOut = window.setTimeout(function() {
					if (!$this.parent().hasClass('open')) {
						$this.click();
					}
				}, 300);
			}
		},
		function() {
			if (timeOut) {
				window.clearTimeout(timeOut);
				timeOut = null;
			}
		});

		$('.page-body .bg, .modal-bg .bg, .cross-sell .bg').imgLiquid();

		$('.dropdown-menu > li > a').on('click', function(e) {
			var $this = $(this);
			if($this.attr('href') == '#') {
				e.preventDefault();
			}
			$this.closest('.dropdown').find('.btn-dropdown > .text').text($this.text());
		});

		$('.droptabs > .dropdown').hide();
		setTimeout(function() {
			$('.droptabs').droptabs();
		}, 20);

		$('#btn-modal-container-close').on('click', function() {
			var $this = $(this);
			var dataTarget = $this.attr('data-target');
			if (typeof dataTarget !== typeof undefined && dataTarget !== false) {
				$(dataTarget).modal('hide');
			}
			else {
				$('.modal').modal('hide');
			}
		});

		$('.modal').on('show.bs.modal', function() {
			collapseMenu();
		});

		$('.modal .panel-collapse').on('shown.bs.collapse', function() {
			var modalID = $(this).parents('.modal').attr('id');
			$('#'+modalID).data('bs.modal').handleUpdate();
		});

		$('.modal').on('hidden.bs.modal', function() {
			$(this).removeData('bs.modal');
		});

		$('.modal-container').on('show.bs.modal', function() {
			$body.addClass('modal-container-open');
		});

		$('.modal-container').on('shown.bs.modal', function() {
			$('html, body').animate({
				scrollTop: 0
			}, 300);
		});

		$('.modal-container').on('hide.bs.modal', function() {
			if($('.modal-container.in').length == 1) {
				$body.removeClass('modal-container-open');
			}
		});

		$('.modal-container').on('hidden.bs.modal', function() {
			$('html, body').animate({
				scrollTop: 0
			}, 300);
		});

		$('.modal-overlay').on('show.bs.modal', function() {
			$body.addClass('modal-overlay-open');
		});

		$('.modal-overlay').on('shown.bs.modal', function() {
			var $this = $(this);
			if($body.hasClass('modal-toggled')) {
				$body.addClass('modal-overlay-open');
				$body.removeClass('modal-toggled');
			}
			$('[data-toggle="modal"]', $this).on('click', function() {
				$body.addClass('modal-toggled');
			});
		});

		$('.modal-overlay').on('hidden.bs.modal', function() {
			$body.removeClass('modal-overlay-open');
		});

		$('.modal-bg').on('show.bs.modal', function() {
			$body.addClass('modal-bg-open');
		});

		$('.modal-bg').on('hidden.bs.modal', function() {
			$body.removeClass('modal-bg-open');
		});

		$('.secondary .btn-toggle').on('click', function() {
			$(this).parent().toggleClass('open');
		});

		$('html.no-mobile .secondary .btn-toggle').hover(function() {
			var $this = $(this);
			if(!$this.parent().hasClass('open') && viewportWidth > 750) {
				timeOut = window.setTimeout(function() {
					$this.click();
				}, 300);
			}
		},
		function() {
			if (timeOut) {
				window.clearTimeout(timeOut);
				timeOut = null;
			}
		});

		$('.btn-back').on('click', function(e) {
			var $this = $(this);
			if($this.attr('href') == '#') {
				e.preventDefault();
				goBack();
			}
		});

		$('a[data-link="third-party"]').on('click', function() {

			var $this = $(this);
			var dataTarget = $this.attr('data-target');
			var dataTargetNoHash = dataTarget.slice(1);
			var $url = $(this).attr('data-url');
			
			$(dataTarget).on('show.bs.modal', function() {
				$body.addClass(dataTargetNoHash+'-open');
				$(dataTarget+' .modal-footer .btn-confirm').attr('href','#');
			});
			
			$(dataTarget).on('shown.bs.modal', function() {
				var $action = $(dataTarget+' .modal-footer .btn-confirm');
				$action.attr('href', $url);
				$action.on('click', function() {
					$(dataTarget).modal('hide');
				});
			});
			
			$(dataTarget).on('hidden.bs.modal', function() {
				$(dataTarget+' .modal-footer .btn-confirm').attr('href','#');
				$body.removeClass(dataTargetNoHash+'-open');
			});
			
		});

		$('[data-modal-iframe]').on('click', function() {
			var $this = $(this);
			var dataTarget = $this.attr('data-target');
			var dataTargetSrc = $this.attr('data-modal-iframe');
			$(dataTarget).on('show.bs.modal', function() {
				var $this = $(this);
				$this.find('iframe').attr('src', dataTargetSrc);
			});
		});

		$('.breadcrumb').addClass('prevent-click');
		$('.breadcrumb.prevent-click a').on('click', function(e) {
			e.preventDefault();
		});

		$('.dropdown .dropdown-menu-input li').on('click', function(e) {
			e.stopPropagation();
		});

		$('footer .general-info').change(function() {
			var $this = $(this);
			var value = $this.val();
			var extension = value.substr((value.lastIndexOf('.') +1));
			if(extension == 'pdf') {
				window.open(value, '_blank');
			}
			else {
				window.location.href = value;
			}
		});

		$('.back-top').on('click', function(e) {
			e.preventDefault();
			$document.animate({
				scrollTop: 0 ,
			}, 300);
		});

		if(window.location.hash) {
			var hash = window.location.hash.substring(1);
			if(hash == 'feedback-form') {
				var modal = '#' + hash;
				$(modal).modal('show');
			}
			else if($html.hasClass('no-mobile')) {
				setTimeout(function(){
					$contentScrollable.mCustomScrollbar('scrollTo', '#'+hash, { scrollInertia: 1200 });
				}, 300);
			}
			else {
				$document.animate({
					scrollTop: $('#' + hash).offset()
				}, 1200);
			}
		}

		$('[data-scrollto]').on('click', function(e) {
			e.preventDefault();
			var $this = $(this);
			var target = $this.attr('data-scrollto');
			if($html.hasClass('no-mobile')) {
				setTimeout(function(){
					$contentScrollable.mCustomScrollbar('scrollTo', target, { scrollInertia: 1200 });
				}, 300);
			}
			else {
				$document.animate({
					scrollTop: $(target).offset().top
				}, 1200);
			}
		});

		$('[data-toggle="tooltip"]').tooltip();

		// var visit = GetCookie("COOKIE1");
		// if (visit == ""){
		// 	$('#classic-view').modal('show');
		// }
		// var expire = new Date();
		// expire = new Date(expire.getTime()+7776000000);
		// document.cookie = "COOKIE1=here; path=/; expires="+expire;

		adjustWindow();

		$('.form-searchbox-hide').hide();
		$('a.btn-search').click(function (e) {
		    e.preventDefault();
		    var offLength = $('.form-inline.form-off').length;
		    if (offLength == 1) {
                //to show
		        $('.form-inline.form-off').removeClass('form-off').addClass('form-on');
		        $(this).find('span:eq(0)').hide();
		        $(this).find('span:eq(1)').show();
		    } else {
		        //to hide
		        $('.form-inline.form-on').removeClass('form-on').addClass('form-off');
		        $(this).find('span:eq(0)').show();
		        $(this).find('span:eq(1)').hide();
		    }
		});

        // When user press Enter Key
		$('#headerSearchTextBox').keypress(function (e) {
		    e.stopImmediatePropagation();
		    if (e.which == 13) {
		        postSearchQuery();
		    }
		});

        // When user click on search button
		$('#searchBtnHeader').click(function (e) {
		    e.preventDefault(); 
		    postSearchQuery();
		});
           
		function postSearchQuery() {
		    // Get some values from elements on the page:
		    var term = "?s=" + $('#headerSearchTextBox').val();

		    // Send the data using post
		    // $.post($('#searchURL').val(), { s: term });


		    var searchURL = $('#searchURL').val();
		    var defaultSearchURL = $('#defaultSearchURL').val();
		    if (searchURL) {
		        window.location.replace($('#searchURL').val() + term);
		    } else { window.location.replace($('#defaultSearchURL').val() + term); }

		}


	});

}(window.jQuery, window, document));
